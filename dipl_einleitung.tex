\section{Einleitung}
\label{sec:einleitung}

\subsection{Aufgabenstellung}
\label{sec:aufgabenstellung}

Die Aufgabenstellung dieser Diplomarbeit dreht sich um die Aussage von Zienkiewicz und Zhu \cite{ZienZhu1987}:

\begin{quote} {\it
The subject of error estimates for finite element solutions and a consequent adaptive analysis, in which the approximation is successively refined to reach predetermined standards of accuracy, is central to the effective use of finite element codes for practical engineering analysis. }
\end{quote}

Eine praktische Anwendung solcher adaptiver Verfahren gliedert sich also in zwei Problemstellungen
\begin{itemize}
\item Sch�tzung des Fehlers, der durch die Anwendung der Diskretisierung bei den Finiten Elementen gemacht wird.
\item Schrittweise Verfeinerung um die vorgegebene Genauigkeit der L�sung gew�hrleisten zu k�nnen.
\end{itemize}

F�r den ersten Punkt sind schnelle und gute Fehlersch�tzer w�nschenswert. Diese {\it a posteriori} Fehlersch�tzer werden in zwei gro�e Gruppen unterteilt, den {\it recovery based error estimator} und den {\it residual based error estimator} \cite{ZienTayl2000}. F�r diese Diplomarbeit wurde der auch als ZZ-Fehlersch�tzer ({\bf Z}ienkiewicz und {\bf Z}hu) bekannte {\it recovery based error estimator} verwendet \cite{ZienZhu1987}.

Die Schwierigkeit des zweiten Punktes besteht in der Entwicklung eines geeigneten Algorithmus zur lokalen Verfeinerung des Netzes. Hierf�r muss eine geeignete Datenstruktur f�r das Netz gew�hlt werden, h�ufig kommen ``hierarchische Netze'' zur Anwendung. Des Weiteren muss bei der Verfeinerung R�cksicht auf die Randgeometrie des Gebietes genommen werden und die G�ltigkeit des Netzes erhalten bleiben, d.h. es sollten keine h�ngenden Knoten entstehen. Es gibt zwei prim�re Varianten zur Verfeinerung des Netzes:

\begin{itemize}
\item {\it h} - Verfeinerung: Hier wird die Gr��e der Elemente durch eine geeignete Unterteilung der Mutterelemente in Tochterelemente reduziert
\item {\it p} - Verfeinerung: Hier wird die Ordnung der Polynome f�r die Ansatzfunktion eines Elementes erh�ht.
\end{itemize}

Diese Arbeit beschr�nkt sich auf den Einsatz der {\it h} - Verfeinerung auf {\it lineare} und {\it quadratische} Dreieckselemente mit Hilfe des {\it recovery based} ZZ-Fehlersch�tzers.

Im Folgenden wird ein kurzer �berblick �ber das in dieser Arbeit angewandte Finite Elemente Verfahren f�r den Spezialfall des linearelastischen Problems gegeben. Ganz zuletzt werden bereits hier besonders beachtenswerte Punkte hervorgehoben, welche sp�ter f�r eine Anwendung eines adaptiven Verfahrens unabdingbar sind.

\subsection{Das linearelastische Problem}
\label{sec:linear_elast_prob}

In dieser Arbeit wird auf die L�sung des linearelastischen Problems - wie in \cite{ZienZhu1987} beschrieben - eingegangen. Dieses Problem ist durch folgende Differentialgleichung formuliert

\begin{equation}
  \label{eq:linear_elastisch}
  {\bf L u - q \equiv S^T D S u - q = 0}
\end{equation}

g�ltig im Gebiet $\Omega$, mit den K�rperkr�ften ${\bf q}$ und der vorgeschriebenen Verschiebung

\begin{equation}
  {\bf u = \bar{u}} \quad \mbox{auf dem Rand} \quad \Gamma_u
\end{equation}

und der vorgeschriebenen Belastung

\begin{equation}
  {\bf G D S u = \bar{t}} \quad \mbox{auf dem Rand} \quad \Gamma_t
\end{equation}

wobei f�r den Rand verlangt wird

\begin{equation}
  \Gamma = \Gamma_u \cup \Gamma_t
\end{equation}

Da f�r diese Spannungen $\boldsymbol{\sigmaup}$ (und nat�rlich auch f�r die Verzerrungen $\boldsymbol{\varepsilonup}$) folgende Voigtsche Notation benutzt wird

\begin{equation}
  \label{eq:voigt}
  \boldsymbol{\sigmaup} = [ \sigma_{xx} , \sigma_{yy}, \sigma_{zz}, \sigma_{xy}, \sigma_{yz}, \sigma_{zx} ]^T
\end{equation}

l�sst sich die Cauchysche Formel \cite{CelFest2004} auch folgenderma�en schreiben ${\bf \bar{t} = G} \boldsymbol{\sigmaup}$:

\begin{equation}
  \label{eq:cauchy}
  \begin{bmatrix}
    \bar{t_x} \\
    \bar{t_y} \\
    \bar{t_z}
  \end{bmatrix} = \myunderbrace{
  \begin{bmatrix}
    n_x & 0 & 0 & n_y & 0 & n_z \\
    0 & n_y & 0 & n_x & n_z & 0 \\
    0 & 0 & n_z & 0 & n_y & n_x \\
  \end{bmatrix}}{\bf G}
  \begin{bmatrix}
    \sigma_{xx} \\
    \sigma_{yy} \\
    \sigma_{zz} \\
    \sigma_{xy} \\
    \sigma_{yz} \\
    \sigma_{zx}
  \end{bmatrix}
\end{equation}

Der Matrixoperator ${\bf S}$ beschreibt die Verzerrungen $\boldsymbol{\varepsilonup}$ als

\begin{equation}
  \label{eq:verzerrung_verschiebung}
  \boldsymbol{\varepsilonup} = {\bf S u }
\end{equation}

und die Elastizit�tsmatrix ${\bf D}$ definiert die Spannungen mit Hilfe des linearelastischen Materialgesetzes nach Hooke (Verzerrungs-Verschiebungs-Gesetz siehe auch \cite{CelFest2004}, \cite{Parkus1998}) als

\begin{equation}
  \label{eq:hooke}
  \boldsymbol{\sigmaup} = {\bf D \boldsymbol{\varepsilonup}}
\end{equation}

\subsection{Die Finite Elemente L�sung - Die Diskretisierung}
\label{sec:fe_approx}

F�r die Finite Elemente L�sung setzt man

\begin{equation}
  \label{eq:form_funktion}
  {\bf u  \approx \hat{u} = N a}
\end{equation}

an, wobei ${\bf a}$ die Knotenpunktsverschiebungen eines Elementes darstellen und ${\bf N}$ die Matrix der Formfunktionen. Durch das Galerkin Verfahren (Multiplikation der Differentialgleichung mit der Testfunktion und Integration �ber das Gebiet $\Omega$) ergibt sich aus (\ref{eq:linear_elastisch})

\begin{equation}
  \label{eq:FE_GLS}
  {\bf K a - f = r}  
\end{equation}
mit
\begin{eqnarray*}
  {\bf K} &=&  \int\limits_{\Omega} {\bf\left( S N \right)^T D \left( S N \right)} \de \Omega \\  
          &=&  \int\limits_{\Omega} {\bf B^T D B} \de \Omega\\
  {\bf f} &=&  \int\limits_{\Omega} {\bf N^T b} \de \Omega + \int\limits_{\Gamma_t} {\bf N^T \bar{t}} \de \Gamma
\end{eqnarray*}

${\bf b}$ sind die verteilten Volumskr�fte und ${\bf r}$ die konzentrierten Knotenkr�fte und ${\bf\bar{t}}$ die verteilte Kraftdichte am Rand $\Gamma_t$ des Gebietes. Bei der Berechnung von ${\bf f}$ ber�cksichtigt man weder Anfangsverzerrungen $\boldsymbol{\varepsilonup}_0$ noch Anfangsspannungen $\boldsymbol{\sigmaup}_0$, weiters ist die Temperatur �ber dem gesamten Gebiet $\Omega$ konstant. Eine genauere Beschreibung des Aufbaus der hier angegebenen Matrizen und Vektoren in dieser Implementierung folgt in Kapitel \ref{sec:anwend_dreieckselement}.

Die Spannungen ergeben sich weiters nach L�sung des FE Gleichungssystems (\ref{eq:FE_GLS}) unter Ber�cksichtigung von (\ref{eq:hooke}) aus

\begin{equation}
  \label{eq:spannungen}
  \boldsymbol{\hat\sigmaup} = {\bf ( D S N ) a}
\end{equation}

Die N�herungsl�sungen ${\bf \hat{u},\boldsymbol{\hat\sigmaup}}$ der schwachen Formulierung (\ref{eq:FE_GLS}) unterscheiden sich von der exakten L�sung ${\bf u, \boldsymbol{\sigmaup}}$ der starken Formulierung (\ref{eq:linear_elastisch}). Diesen Unterschied bezeichnen wir als Fehler. Dies bedeutet f�r die Verschiebungen, Verzerrungen und Spannungen

\begin{eqnarray}
  \label{eq:error}
  {\bf e} &=& {\bf u - \hat u} \nonumber \\
  {\bf e}_\varepsilon &=& \boldsymbol{\varepsilonup} - \boldsymbol{\hat\varepsilonup} \nonumber \\
  {\bf e}_\sigma &=& \boldsymbol{\sigmaup} - \boldsymbol{\hat\sigmaup}
\end{eqnarray}


\subsubsection{Hinweise f�r die Anwendung adaptiver FE Methoden}
\label{sec:hinw_adapt_verfahren}

\paragraph{Randbedingungen}

F�r die L�sung des Gleichungssystems (\ref{eq:FE_GLS}) ist es erforderlich Randbedingungen einzubauen. Dies bedeutet im Prinzip Freiheitsgrade des Systems vorzugeben oder zu koppeln, sodass das Gleichungssystem regul�r und somit l�sbar wird. 

Sei nun ${\bar \omega}_h^{\Gamma_u}$ die Indexmenge aller auf $\Gamma_u$ liegenden Knoten, wobei $\Gamma_u \subset \Gamma$ jene Teilmenge darstellt auf der Randbedinungen 1. Art (z.B. Verschiebungen) gegeben sind. Ist die L�sung f�r ein gegebenes Netz  (eine g�ltige Diskretisierung ${\bar \Omega}_h$ des Gebietes $\Omega$) gefragt, so kann man relativ einfach die entsprechenden Werte bzw. gegebenen Zusammenh�nge der Freiheitsgrade der Knoten ${\bar \omega}_h^{\Gamma_u}$ in das GLS (\ref{eq:FE_GLS}) einbauen. Hierf�r gibt es verschiedene M�glichkeiten, auf die hier nicht n�her eingegangen wird, siehe \cite{ZienTayl2000} bzw. \cite{Bathe1996}.

Wenn nun aber das Ursprungsnetz ${}^0{\bar \Omega}_h$ eine Verfeinerung erf�hrt (wie es bei adaptiven Verfahren der Fall ist), so wird f�r den allgemeinen Fall das neue Netz ${}^1{\bar \Omega}_h$ eine neue Indexmenge ${}^1{\bar \omega}_h^{\Gamma_u}$ von Knoten auf $\Gamma_u$ aufweisen, welche nicht ident ist mit ${}^0{\bar \omega}_h^{\Gamma_u}$. Dies bedeutet mit anderen Worten, dass die Randbedingung eine Eigenschaft des Randes $\Gamma_u$ ist und nicht eine Eigenschaft der Knoten. Es ist daher nicht m�glich, die Randbedingungen als Eigenschaft der Knoten in das Ursprungsnetz zu �bertragen, man muss bereits f�r den Inputdatensatz geometrische Randbereiche $\Gamma_{u,i}$ (im Fall von 2D Elementen sind dies einparametrische Kurvenst�cke, Strecken, Kreise, usw.) definieren, an denen Randbedingungen gegeben sind.

\paragraph{Lasten}

�hnliches wie f�r Randbedingungen gilt auch f�r Lasten. Da es pysikalisch gesehen unm�glich ist, Einzelkr�fte an Punkten mit unendlich kleiner Ausdehnung anzubringen, ist auch die ``Kraft'' oder besser Kraftdichte ${\bf \bar t}$ als Eigenschaft des Randbereiches $\Gamma_t$ zu verstehen. Gilt es mehr als nur das Stammnetz ${}^0{\bar \Omega}_h$ zu rechnen auf dem die Indexmenge ${}^0{\bar \omega}_h^{\Gamma_t}$ durchaus im vorhinein gegeben sein mag, so muss die Information der �u�eren Lasten im Imputdatensatz an die Randbereiche $\Gamma_{t,i}$ gekoppelt werden. 