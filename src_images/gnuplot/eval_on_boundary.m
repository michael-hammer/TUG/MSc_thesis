clear;
close all;

ref_depth = 3;
read_dir='kirsch_2/';
coord = 3; % x = 2, y = 3
evaluate_sigma = 1; % 1 ... erste Koordinate (rr,xx), 2 ... zweite Koordinate(phiphi,yy)

color = ['r' , 'g' , 'b' , 'c' , 'm' , 'y' , 'b'];

% Für th_cyl_1
% boundary_array = [1 4 7 10 19 26 31 56 61 82 87 93 97 190 195 199 275 280 286 287 306 585 590]
% Für th_cyl_2
% boundary_array = [8 14 15 16 17 18 19 84 85 149 150 181 182 289 290 300 301 527 528 538 539 660 661 669 670 1514 1515 1527 1528 1546 1547 1558 1559 1888 1889 1899 1900 1923 1924];
% Für kirsch_1
%if( coord == 2 )
%    % x-Richtung
%    boundary_array = [11 13 17 21 24 37 45 55 81 123 127 151 155 185 256 261 268 430 434 439 443 528 532 538 744 750 1322 1326 1811 1830 1833 1835 1846 1847 1868 1869 1870 1871 1872 1873 1886 1887 1888 1889 1917];
%else
%    % in y-Richtung
%    boundary_array = [12 14 18 22 25 34 42 54 79 108 111 136 139 179 260 265 266 374 377 389 739 740 741 742 743 749 1194 1197 1207 1212 1831 1832 1843 1844 1845 1860 1903 1904 1905 1911 1920];
%end 
% Für kirsch_2
if( coord == 2 )
    % x-Richtung
    boundary_array = [42 44 48 53 59 64 71 77 82 121 122 149 150 194 195 282 283 443 444 454 455 547 548 558 559 685 686 696 697 886 887 1015 1016 1485 1486 1496 1497 1515 1516 1529 1530 1719 1720 1730 1731 1745 1746];
else
    % in y-Richtung
    boundary_array = [43 45 49 54 60 65 72 78 83 109 110 137 138 182 183 234 235 386 387 398 399 490 491 502 503 659 660 664 665 1307 1308 1319 1320 1361 1362 1606 1607 1618 1619 1989 1990];
end 

figure(1);
    hold on;

for i = 0:ref_depth
    clear data_set plot_data;
    data_set_counter = 1;
    
    result = importdata([read_dir 'node_res_' int2str(i) '.txt']);
    msh = importdata([read_dir 'node_msh.txt']);
    for j = 1:length(boundary_array)
        data = findNodeValueInArray(boundary_array(j),3+evaluate_sigma,result);
        if data(2) > 0.5 
            data_set(data_set_counter,2) = data(1);
            coord_value = findNodeValueInArray(boundary_array(j),coord,msh);
            data_set(data_set_counter,1) = coord_value(1);
            data_set_counter = data_set_counter + 1;
        end
    end
    [B,index] = sort(data_set);
    for j = 1:length(index(:,1))
        plot_data(j,:) = data_set(index(j,1),:);
    end

    % Hier muss die richtige Spannungsfunktion gewaehlt werden!
    % Für thick_cylinder
    %    sigma = thick_cylinder(plot_data(:,1))
    %    delta = plot_data(:,2) - sigma(:,evaluate_sigma)
    % Für kirsch
    if( coord == 2 )
        phi = 0;
    else
        phi = pi/2;
    end
    sigma = kirsch(plot_data(:,1),phi)
    if( coord == 2 )  
        delta = plot_data(:,2) - sigma(:,evaluate_sigma);
    else
        delta = plot_data(:,2) - sigma(:,3-evaluate_sigma);
    end
    
    plot_data
    
    plot(plot_data(:,1), delta, color(i+1) );
end
legend('refine0','refine1','refine2','refine3','refine4');

