function value = findNodeValueInArray(node_number, column, array)
for i = 1:length(array(:,1))
    if array(i,1) == node_number
        value = [array(i,column) 1];
        return;
    end
end
value = [0 0];
