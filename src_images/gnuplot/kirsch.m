function sigma = kirsch(r,phi)
% This is the help
a = 0.5;
p = 1;
h = 1;
for i = 1:length(r)
    % sigma_rr
    sigma(i,1) = p/(2*h) * (1-a^2/r(i)^2 + (1-4*a^2/r(i)^2+3*a^4/r(i)^4)*cos(2*phi));
    % sigma_phiphi
    sigma(i,2) = p/(2*h) * (1+a^2/r(i)^2 - (1+3*a^4/r(i)^4)*cos(2*phi));
end