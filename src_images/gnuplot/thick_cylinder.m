function sigma = thick_cylinder(r)
% This is the help
a = 20;
b = 5;
p = 1;
h = 1;
for i = 1:length(r)
    sigma(i,1) = -p/h * b^2/(b^2-a^2)*(1-a^2/r(i)^2);
    sigma(i,2) = -p/h * b^2/(b^2-a^2)*(1+a^2/r(i)^2);
end